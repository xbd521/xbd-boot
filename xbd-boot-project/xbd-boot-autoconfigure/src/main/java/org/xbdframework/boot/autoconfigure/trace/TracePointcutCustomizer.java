package org.xbdframework.boot.autoconfigure.trace;

import org.springframework.aop.aspectj.AspectJExpressionPointcut;

/**
 * 应用执行轨迹追踪自动化配置{@link org.springframework.aop.aspectj.AspectJExpressionPointcut}个性化接口.
 * @author luas
 * @since 1.5.0
 */
@FunctionalInterface
public interface TracePointcutCustomizer {

    /**
     * 用以对{@link org.springframework.aop.aspectj.AspectJExpressionPointcut}进行个性化配置.
     * @param pointcut {@link AspectJExpressionPointcut} 类型{@code Pointcut}.
     * @see org.springframework.aop.aspectj.AspectJExpressionPointcut#setExpression(String)
     * @see org.springframework.aop.aspectj.AspectJExpressionPointcut#setParameterNames(String...)
     * @see org.springframework.aop.aspectj.AspectJExpressionPointcut#setParameterTypes(Class[])
     */
    void customize(AspectJExpressionPointcut pointcut);
}
