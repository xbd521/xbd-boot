package org.xbdframework.boot.autoconfigure.trace;

import java.lang.reflect.Method;

import org.xbdframework.trace.interceptor.MethodMapTraceAttributeSource;
import org.xbdframework.trace.interceptor.TraceAttribute;

/**
 * 应用执行轨迹追踪自动化配置{@link org.xbdframework.trace.interceptor.MethodMapTraceAttributeSource}个性化接口.
 * @author luas
 * @since 1.5.0
 */
public interface TraceMethodMapCustomizer {

    /**
     * 用以声明个性化{@link org.xbdframework.trace.interceptor.MethodMapTraceAttributeSource}.
     * @return {@link org.xbdframework.trace.interceptor.MethodMapTraceAttributeSource}.
     * @see org.xbdframework.trace.interceptor.MethodMapTraceAttributeSource
     * @see org.xbdframework.trace.interceptor.MethodMapTraceAttributeSource#addTraceableMethod(String, TraceAttribute)
     * @see org.xbdframework.trace.interceptor.MethodMapTraceAttributeSource#addTraceableMethod(Method, TraceAttribute)
     * @see org.xbdframework.trace.interceptor.MethodMapTraceAttributeSource#addTraceableMethod(Class, String, TraceAttribute)
     */
    MethodMapTraceAttributeSource customize();
}
