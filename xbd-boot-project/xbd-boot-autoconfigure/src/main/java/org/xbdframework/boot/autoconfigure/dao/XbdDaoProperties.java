package org.xbdframework.boot.autoconfigure.dao;

import org.xbdframework.dao.DbType;
import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(prefix = "xbd.dao")
public class XbdDaoProperties {

	private DbType dbType = DbType.MYSQL;

	private boolean showSql = true;

	public DbType getDbType() {
		return dbType;
	}

	public void setDbType(DbType dbType) {
		this.dbType = dbType;
	}

	public boolean isShowSql() {
		return showSql;
	}

	public void setShowSql(boolean showSql) {
		this.showSql = showSql;
	}

}
