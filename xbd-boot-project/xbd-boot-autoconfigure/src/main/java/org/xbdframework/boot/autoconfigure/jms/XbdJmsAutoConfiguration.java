package org.xbdframework.boot.autoconfigure.jms;

import javax.jms.ConnectionFactory;
import javax.jms.Message;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.jms.JmsAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jms.config.DefaultJmsListenerContainerFactory;
import org.springframework.jms.config.JmsListenerContainerFactory;
import org.springframework.jms.listener.DefaultMessageListenerContainer;

/**
 * Jms自动化配置
 *
 * @author luas
 * @since 1.5.0
 */
@Configuration
@ConditionalOnClass({ Message.class, JmsTemplate.class })
@ConditionalOnBean(ConnectionFactory.class)
@AutoConfigureAfter(JmsAutoConfiguration.class)
public class XbdJmsAutoConfiguration {

	public static final String DEFAULT_JMS_LISTENER_CONTAINER_TOPIC_CONCURRENCY = "1";

	@Bean
	@ConditionalOnMissingBean(name = "topicJmsListenerContainerFactory")
	public JmsListenerContainerFactory<DefaultMessageListenerContainer> topicJmsListenerContainerFactory(
			ConnectionFactory connectionFactory) {
		DefaultJmsListenerContainerFactory defaultJmsListenerContainerFactory = new DefaultJmsListenerContainerFactory();

		defaultJmsListenerContainerFactory.setConnectionFactory(connectionFactory);
		defaultJmsListenerContainerFactory
				.setConcurrency(DEFAULT_JMS_LISTENER_CONTAINER_TOPIC_CONCURRENCY);
		defaultJmsListenerContainerFactory.setPubSubDomain(true);

		return defaultJmsListenerContainerFactory;
	}

}
