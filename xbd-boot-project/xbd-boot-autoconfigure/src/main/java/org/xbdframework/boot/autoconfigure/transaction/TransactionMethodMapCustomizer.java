package org.xbdframework.boot.autoconfigure.transaction;

import org.springframework.transaction.interceptor.MethodMapTransactionAttributeSource;

/**
 * 用于配置特殊化事务拦截规则
 *
 * @author luas
 * @since 1.5.0
 */
@FunctionalInterface
public interface TransactionMethodMapCustomizer {

	/**
	 * 返回{@link MethodMapTransactionAttributeSource}形式的事务拦截规则
	 * @return
	 */
	MethodMapTransactionAttributeSource customize();

}
