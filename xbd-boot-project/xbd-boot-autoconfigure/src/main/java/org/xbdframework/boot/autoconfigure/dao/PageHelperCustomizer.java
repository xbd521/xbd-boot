package org.xbdframework.boot.autoconfigure.dao;

import org.xbdframework.dao.PageHelper;

/**
 * {@link org.xbdframework.dao.PageHelper} 个性化配置接口
 *
 * @author luas
 * @since 1.5.0
 */
@FunctionalInterface
public interface PageHelperCustomizer {

    /**
     * 返回自定义的 {@link org.xbdframework.dao.PageHelper} 实例
     * @return 自定义的 {@link org.xbdframework.dao.PageHelper} 实例
     */
    PageHelper customize();

}
