package org.xbdframework.boot.autoconfigure.trace;

import org.xbdframework.trace.annotation.AnnotationTraceAttributeSource;
import org.xbdframework.trace.annotation.DefaultTraceAnnotationParser;
import org.xbdframework.trace.annotation.Traceable;
import org.xbdframework.trace.interceptor.TraceAttributeSourceAdvisor;
import org.xbdframework.trace.interceptor.TraceInterceptor;

import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.context.annotation.Configuration;

/**
 * 使用注解{@link org.xbdframework.trace.annotation.Traceable}的运行轨迹追踪记录自动化配置.
 * <p>{@code TraceAttributeSource}为{@link AnnotationTraceAttributeSource}.
 * @author luas
 * @since 1.5.0
 * @see AnnotationTraceAttributeSource
 * @see TraceAttributeSourceAdvisor
 * @see org.xbdframework.trace.interceptor.TraceAttributeSourcePointcut
 */
@Configuration
@ConditionalOnClass(Traceable.class)
@AutoConfigureAfter(TraceAutoConfiguration.class)
public class AnnotationTraceAutoConfiguration {

    @Bean
    public TraceInterceptor annotationTraceInterceptor() {
        TraceInterceptor interceptor = new TraceInterceptor();

        AnnotationTraceAttributeSource traceAttributeSource = new AnnotationTraceAttributeSource(new DefaultTraceAnnotationParser());

        interceptor.setTraceAttributeSource(traceAttributeSource);

        return interceptor;
    }

    @Bean
    public TraceAttributeSourceAdvisor annotationTraceAttributeSourceAdvisor() {
        TraceAttributeSourceAdvisor advisor = new TraceAttributeSourceAdvisor();
        advisor.setTraceInterceptor(annotationTraceInterceptor());
        return advisor;
    }

}
