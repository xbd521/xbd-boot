package org.xbdframework.boot.autoconfigure.quartz;

import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * Quartz属性配置
 *
 * @author luas
 * @since 1.5.0
 */
@ConfigurationProperties(prefix = "xbd.quartz")
public class XbdQuartzProperties {

	private DataSourceType dataSourceType = DataSourceType.SYSTEM;

	private String applicationContextSchedulerContextKey = "applicationContext";

	public DataSourceType getDataSourceType() {
		return dataSourceType;
	}

	public void setDataSourceType(DataSourceType dataSourceType) {
		this.dataSourceType = dataSourceType;
	}

	public String getApplicationContextSchedulerContextKey() {
		return applicationContextSchedulerContextKey;
	}

}
