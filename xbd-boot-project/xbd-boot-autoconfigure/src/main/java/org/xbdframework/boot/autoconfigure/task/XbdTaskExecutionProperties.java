package org.xbdframework.boot.autoconfigure.task;

import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * Configuration properties for task execution.
 *
 * @author luas
 * @since 1.5.0
 */
@ConfigurationProperties("xbd.task.execution")
public class XbdTaskExecutionProperties {

	private final Pool pool = new Pool();

	private String threadGroupName = "group-";

	public Pool getPool() {
		return this.pool;
	}

	public String getThreadGroupName() {
		return threadGroupName;
	}

	public void setThreadGroupName(String threadGroupName) {
		this.threadGroupName = threadGroupName;
	}

	public static class Pool {

		/**
		 * Set whether to wait for scheduled tasks to complete on shutdown, not
		 * interrupting running tasks and executing all tasks in the queue.
		 * <p>
		 * Default is "false", shutting down immediately through interrupting ongoing
		 * tasks and clearing the queue. Switch this flag to "true" if you prefer fully
		 * completed tasks at the expense of a longer shutdown phase.
		 * <p>
		 * Note that Spring's container shutdown continues while ongoing tasks are being
		 * completed. If you want this executor to block and wait for the termination of
		 * tasks before the rest of the container continues to shut down - e.g. in order
		 * to keep up other resources that your tasks may need -, set the
		 * {@link org.springframework.scheduling.concurrent.ExecutorConfigurationSupport#setAwaitTerminationSeconds
		 * "awaitTerminationSeconds"} property instead of or in addition to this property.
		 * @see java.util.concurrent.ExecutorService#shutdown()
		 * @see java.util.concurrent.ExecutorService#shutdownNow()
		 */
		private boolean waitForTasksToCompleteOnShutdown = false;

		/**
		 * Set the maximum number of seconds that this executor is supposed to block on
		 * shutdown in order to wait for remaining tasks to complete their execution
		 * before the rest of the container continues to shut down. This is particularly
		 * useful if your remaining tasks are likely to need access to other resources
		 * that are also managed by the container.
		 * <p>
		 * By default, this executor won't wait for the termination of tasks at all. It
		 * will either shut down immediately, interrupting ongoing tasks and clearing the
		 * remaining task queue - or, if the
		 * {@link org.springframework.scheduling.concurrent.ExecutorConfigurationSupport#setWaitForTasksToCompleteOnShutdown
		 * "waitForTasksToCompleteOnShutdown"} flag has been set to {@code true}, it will
		 * continue to fully execute all ongoing tasks as well as all remaining tasks in
		 * the queue, in parallel to the rest of the container shutting down.
		 * <p>
		 * In either case, if you specify an await-termination period using this property,
		 * this executor will wait for the given time (max) for the termination of tasks.
		 * As a rule of thumb, specify a significantly higher timeout here if you set
		 * "waitForTasksToCompleteOnShutdown" to {@code true} at the same time, since all
		 * remaining tasks in the queue will still get executed - in contrast to the
		 * default shutdown behavior where it's just about waiting for currently executing
		 * tasks that aren't reacting to thread interruption.
		 * @see java.util.concurrent.ExecutorService#shutdown()
		 * @see java.util.concurrent.ExecutorService#awaitTermination
		 */
		private int awaitTerminationSeconds = 0;

		public boolean isWaitForTasksToCompleteOnShutdown() {
			return waitForTasksToCompleteOnShutdown;
		}

		public void setWaitForTasksToCompleteOnShutdown(
				boolean waitForTasksToCompleteOnShutdown) {
			this.waitForTasksToCompleteOnShutdown = waitForTasksToCompleteOnShutdown;
		}

		public int getAwaitTerminationSeconds() {
			return awaitTerminationSeconds;
		}

		public void setAwaitTerminationSeconds(int awaitTerminationSeconds) {
			this.awaitTerminationSeconds = awaitTerminationSeconds;
		}

	}

}
