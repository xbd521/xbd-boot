package org.xbdframework.boot.autoconfigure.quartz;

import org.xbdframework.scheduling.quartz.XbdQuartzJobBean;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.SchedulerException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;

/**
 * Quartz默认JobBean
 *
 * @author luas
 * @since 1.5.0
 */
public class DefaultXbdQuartzJobBean extends XbdQuartzJobBean {

	@Autowired
	private XbdQuartzProperties properties;

	@Override
	protected void executeInternal(JobExecutionContext context)
			throws JobExecutionException {
		try {
			ApplicationContext ctx = (ApplicationContext) context.getScheduler()
					.getContext()
					.get(this.properties.getApplicationContextSchedulerContextKey());

			Object bean = createJobInstance(ctx);

			executeInternal(context, bean);
		}
		catch (SchedulerException ex) {
			throw new JobExecutionException(ex.getMessage(), ex);
		}
	}

}
