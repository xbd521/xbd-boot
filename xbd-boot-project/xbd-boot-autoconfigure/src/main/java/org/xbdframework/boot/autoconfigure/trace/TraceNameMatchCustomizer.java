package org.xbdframework.boot.autoconfigure.trace;

import org.xbdframework.trace.interceptor.TraceAttribute;

import java.util.Map;

/**
 * 应用执行轨迹追踪自动化配置{@link org.xbdframework.trace.interceptor.NameMatchTraceAttributeSource}个性化接口.
 * @author luas
 * @since 1.5.0
 */
public interface TraceNameMatchCustomizer {

    /**
     * 用以向{@link org.xbdframework.trace.interceptor.NameMatchTraceAttributeSource}添加个性化 traceable method attribute.
     * @return 需要个性化的traceable method attribute.
     * @see org.xbdframework.trace.interceptor.NameMatchTraceAttributeSource#addTraceableMethod(String, TraceAttribute)
     */
    Map<String, TraceAttribute> customize();
}
