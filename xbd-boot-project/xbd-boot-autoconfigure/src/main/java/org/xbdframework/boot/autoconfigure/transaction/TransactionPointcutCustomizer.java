package org.xbdframework.boot.autoconfigure.transaction;

import org.springframework.aop.aspectj.AspectJExpressionPointcut;

/**
 * 用于覆盖统一自动化事务配置中的AspectJ Expression表达式
 *
 * @author luas
 * @since 1.5.0
 */
@FunctionalInterface
public interface TransactionPointcutCustomizer {

	/**
	 * 覆盖统一自动化事务配置中的AspectJ Expression表达式
	 * @param pointcut {@link AspectJExpressionPointcut}
	 */
	void customize(AspectJExpressionPointcut pointcut);

}
