package org.xbdframework.boot.autoconfigure.quartz;

/**
 * Quartz数据源类型
 *
 * @author luas
 * @since 1.5.0
 */
public enum DataSourceType {

	/**
	 * 与应用共用同一个数据源，此时quartz对应的数据结构应同步到应用数据源中
	 */
	SYSTEM,

	/**
	 * 通过quartz.properties指定quartz所使用的数据源
	 */
	PROVIDED

}
