package org.xbdframework.boot.autoconfigure.transaction;

import java.util.Map;

import org.springframework.transaction.interceptor.TransactionAttribute;

/**
 * 用于个性化配置统一自动化事务配置中的事务方法拦截规则
 * <p>
 * 原则上不允许覆盖默认配置过的拦截方法，只能定义额外的拦截方法及对应的{@link TransactionAttribute}
 *
 * @author luas
 * @since 1.5.0
 */
@FunctionalInterface
public interface TransactionNameMatchCustomizer {

	/**
	 * 返回事务拦截规则
	 * @return 事务拦截规则
	 */
	Map<String, TransactionAttribute> customize();

}
