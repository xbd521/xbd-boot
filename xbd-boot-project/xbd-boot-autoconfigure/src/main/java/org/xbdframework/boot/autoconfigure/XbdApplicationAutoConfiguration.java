package org.xbdframework.boot.autoconfigure;

import org.xbdframework.context.XbdApplicationEventPublisher;
import org.xbdframework.context.support.DefaultXbdApplicationEventPublisher;
import org.xbdframework.errorhandler.LoggingErrorHandler;
import org.xbdframework.errorhandler.ThrowExceptionErrorHandler;
import org.springframework.boot.autoconfigure.AutoConfigureOrder;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.Ordered;
import org.springframework.util.ErrorHandler;

/**
 * 应用上下文自动化配置
 *
 * @author luas
 * @since 1.5.0
 */
@Configuration
@AutoConfigureOrder(Ordered.HIGHEST_PRECEDENCE + 20)
public class XbdApplicationAutoConfiguration {

	@Configuration
	protected static class XbdApplicationContextConfiguration {

		@Bean
		@ConditionalOnMissingBean(XbdApplicationEventPublisher.class)
		public XbdApplicationEventPublisher defaultXbdApplicationEventPublisher() {
			return new DefaultXbdApplicationEventPublisher();
		}

		@Bean
		@ConditionalOnMissingBean(name = "loggingErrorHandler")
		public ErrorHandler loggingErrorHandler() {
			return new LoggingErrorHandler();
		}

		@Bean
		@ConditionalOnMissingBean(name = "throwExceptionErrorHandler")
		public ErrorHandler throwExceptionErrorHandler() {
			return new ThrowExceptionErrorHandler();
		}

	}

}
