# xbd-boot

基于spring boot的二次封装框架，封装了一些常用的框架的自动化配置

## 与SpringBoot版本关系

总体原则，同步更新，与SpringBoot版本保持一致

<table>
    <tr>
        <td>xbd-boot</td>
        <td>spring-boot</td>
        <td>开发状态</td>
    </tr>
    <tr>
        <td>1.0.x</td>
        <td>1.0.x</td>
        <td>放弃</td>
    </tr>
    <tr>
        <td>1.1.x</td>
        <td>1.1.x</td>
        <td>放弃</td>
    </tr>
    <tr>
        <td>1.2.x</td>
        <td>1.2.x</td>
        <td>放弃</td>
    </tr>
    <tr>
        <td>1.3.x</td>
        <td>1.3.x</td>
        <td>放弃</td>
    </tr>
    <tr>
        <td>1.4.x</td>
        <td>1.4.x</td>
        <td>放弃</td>
    </tr>
    <tr>
        <td>1.5.x</td>
        <td>1.5.x</td>
        <td>开发中</td>
    </tr>
    <tr>
        <td>2.0.x</td>
        <td>2.0.x</td>
        <td>开发中</td>
    </tr>
    <tr>
        <td>2.1.x</td>
        <td>2.1.x</td>
        <td>开发中</td>
    </tr>
</table>

## 与Xbd Framework版本关系

从Spring Framework 4.0开始，之前的版本不在处理

<table>
    <tr>
        <td>xbd-boot</td>
        <td>xbd-framework</td>
        <td>开发状态</td>
    </tr>
    <tr>
        <td>1.0.x</td>
        <td>4.0.3</td>
        <td>放弃</td>
    </tr>
    <tr>
        <td>1.1.x</td>
        <td>4.0.x</td>
        <td>放弃</td>
    </tr>
    <tr>
        <td>1.2.x</td>
        <td>4.1.x</td>
        <td>放弃</td>
    </tr>
    <tr>
        <td>1.3.x</td>
        <td>1.2.x</td>
        <td>放弃</td>
    </tr>
    <tr>
        <td>1.4.x</td>
        <td>4.3.x</td>
        <td>放弃</td>
    </tr>
    <tr>
        <td>1.5.x</td>
        <td>4.3.x</td>
        <td>规划中</td>
    </tr>
    <tr>
        <td>2.0.x</td>
        <td>5.0.x</td>
        <td>开发中</td>
    </tr>
    <tr>
        <td>2.1.x</td>
        <td>5.1.x</td>
        <td>规划中</td>
    </tr>
</table>

## 起步配置
```xml
<parent>
     <groupId>org.xbdframework.boot</groupId>
     <artifactId>xbd-boot-starter-parent</artifactId>
     <version>revision</version>
</parent>
```

## xbd-boot-starter
起步Starter
```xml
<dependency>
    <groupId>org.xbdframework.boot</groupId>
    <artifactId>xbd-boot-starter</artifactId>
</dependency>
```

### 事务配置

#### 系统定义事务拦截路径

TransactionPointcutCustomizer

```java
@Bean
public TransactionPointcutCustomizer pointcutCustomizer() {
	return (pointcut) -> { pointcut.setExpression("execution(* org.xbdframework..service..impl.*.*(..))") };
}
```

#### 系统定义拦截方法

##### 更新事务

* save
* delete
* update
* create
* exec

##### 只读事务

* get
* select
* query
* find 
* list
* count
* is

#### 事务个性化配置

TransactionPointcutCustomizer

TransactionNameMatchCustomizer

TransactionMethodMapCustomizer

#### 特殊事务配置

TransactionMethodMapCustomizer

## xbd-boot-starter-activemq
ActiveMQ Starter
```xml
<dependency>
    <groupId>org.xbdframework.boot</groupId>
    <artifactId>xbd-boot-starter-activemq</artifactId>
</dependency>
```

## xbd-boot-starter-amqp
RabbitMQ Starter
```xml
<dependency>
    <groupId>org.xbdframework.boot</groupId>
    <artifactId>xbd-boot-starter-amqp</artifactId>
</dependency>
```

## xbd-boot-starter-enjoy

enjoy模板引擎Starter
```xml
<dependency>
    <groupId>org.xbdframework.boot</groupId>
    <artifactId>xbd-boot-starter-enjoy</artifactId>
</dependency>
```

### xbd.enjoy
<table>
    <tr>
        <td>配置项</td>
        <td>说明</td>
        <td>默认值</td>
    </tr>
    <tr>
        <td>devMode</td>
        <td>开发模式</td>
        <td>false</td>
    </tr>
    <tr>
        <td>prefix</td>
        <td>模板前缀</td>
        <td>无</td>
    </tr>
    <tr>
        <td>suffix</td>
        <td>模板后缀</td>
        <td>.html</td>
    </tr>
    <tr>
        <td>contentType</td>
        <td>contentType</td>
        <td>text/html</td>
    </tr>
    <tr>
        <td>templatePath</td>
        <td>模板路径</td>
        <td>classpath:/templates/</td>
    </tr>
    <tr>
        <td>checkTemplateLocation</td>
        <td>模板路径检查</td>
        <td>true</td>
    </tr>
    <tr>
        <td>exposeRequestAttributes</td>
        <td></td>
        <td>false</td>
    </tr>
    <tr>
        <td>allowRequestOverride</td>
        <td></td>
        <td>false</td>
    </tr>
    <tr>
        <td>exposeSessionAttributes</td>
        <td></td>
        <td>false</td>
    </tr>
    <tr>
        <td>allowSessionOverride</td>
        <td></td>
        <td>false</td>
    </tr>
    <tr>
        <td>exposeSpringMacroHelpers</td>
        <td></td>
        <td>false</td>
    </tr>
    <tr>
        <td>sessionInView</td>
        <td></td>
        <td>false</td>
    </tr>
    <tr>
        <td>createSession</td>
        <td></td>
        <td>false</td>
    </tr>
</table>

## xbd-boot-starter-jdbc
Jdbc Starter
```xml
<dependency>
    <groupId>org.xbdframework.boot</groupId>
    <artifactId>xbd-boot-starter-jdbc</artifactId>
</dependency>
```

### xbd.dao
<table>
    <tr>
        <td>配置项</td>
        <td>说明</td>
        <td>默认值</td>
    </tr>
    <tr>
        <td>dbType</td>
        <td>数据库类型</td>
        <td>MYSQL</td>
    </tr>
    <tr>
        <td>showSql</td>
        <td>是否显示sql</td>
        <td>true</td>
    </tr>
</table>

## xbd-boot-starter-log4j2
log4j2 Starter
```xml
<dependency>
    <groupId>org.xbdframework.boot</groupId>
    <artifactId>xbd-boot-starter-log4j2</artifactId>
</dependency>
```
提供4种日志配置
* log4j2/log4j2-spring.xml
* log4j2/log4j2-spring-dev.xml
* log4j2/log4j2-spring-test.xml
* log4j2/log4j2-spring-prod.xml

```yaml
logging:
  config: classpath:log4j2/log4j2-spring-dev.xml
  path: d:\logs
```

log4j2-spring.xml
```xml
<!--设置log4j2的自身log级别为warn -->
<configuration status="WARN" monitorInterval="300">

    <properties>
        <property name="logPath">${sys:LOG_PATH}</property>
    </properties>

    <Appenders>
        <Console name="Console" target="SYSTEM_OUT">
            <PatternLayout pattern="[%d{yyyy-MM-dd HH:mm:ss:SSS}] [%p] - %l - %m%n"/>
        </Console>

        <RollingFile name="RollingFile" fileName="${logPath}/info.log"
                     filePattern="${logPath}/$${date:yyyy-MM-dd}/info-%d{yyyy-MM-dd}-%i.log">
            <PatternLayout pattern="[%d{yyyy-MM-dd HH:mm:ss:SSS}] [%p] - %l - %m%n"/>
            <Policies>
                <TimeBasedTriggeringPolicy/>
                <SizeBasedTriggeringPolicy size="20 MB"/>
            </Policies>
            <DefaultRolloverStrategy max="1000">
                <!-- 删除指定目录下7天之前的日志-->
                <!--
                <Delete basePath="${logPath}" maxDepth="2">
                    <IfFileName glob="*/*.log" />
                    <IfLastModified age="180d" />
                </Delete>
                -->
            </DefaultRolloverStrategy>
        </RollingFile>
    </Appenders>

    <Loggers>
        <!--过滤掉springframework和hibernate的一些无用的debug信息 -->
        <!-- springframework的日志级别继承Root的日志级别，即Root为INFO，springframework即为INFO，以此类推 -->
        <AsyncLogger name="org.springframework" level="INFO"></AsyncLogger>
        <AsyncLogger name="org.springframework.boot.web.servlet" level="ERROR"></AsyncLogger>
        <AsyncLogger name="org.springframework.context.support" level="ERROR"></AsyncLogger>
        <AsyncLogger name="org.springframework.jdbc.datasource" level="INFO"></AsyncLogger>
        <AsyncLogger name="org.springframework.web.servlet" level="ERROR"></AsyncLogger>
        <AsyncLogger name="org.elasticsearch" level="INFO"></AsyncLogger>
        <AsyncLogger name="org.apache.activemq" level="INFO"></AsyncLogger>
        <AsyncLogger name="net.sf.ehcache" level="INFO"></AsyncLogger>
        <AsyncLogger name="org.hibernate" level="INFO"></AsyncLogger>

        <AsyncRoot level="INFO" includeLocation="true">
            <AppenderRef ref="Console"/>
            <AppenderRef ref="RollingFile"/>
        </AsyncRoot>
    </Loggers>

</configuration>
```

log4j2-spring-dev.xml
```xml
<!--设置log4j2的自身log级别为warn -->
<configuration status="WARN" monitorInterval="300">

    <properties>
        <property name="logPath">${sys:LOG_PATH}</property>
    </properties>

    <Appenders>
        <Console name="Console" target="SYSTEM_OUT">
            <PatternLayout pattern="[%d{yyyy-MM-dd HH:mm:ss:SSS}] [%p] - %l - %m%n"/>
        </Console>

        <RollingFile name="RollingFile" fileName="${logPath}/info.log"
                     filePattern="${logPath}/$${date:yyyy-MM-dd}/info-%d{yyyy-MM-dd}-%i.log">
            <PatternLayout pattern="[%d{yyyy-MM-dd HH:mm:ss:SSS}] [%p] - %l - %m%n"/>
            <Policies>
                <TimeBasedTriggeringPolicy/>
                <SizeBasedTriggeringPolicy size="20 MB"/>
            </Policies>
            <DefaultRolloverStrategy max="1000">
                <!-- 删除指定目录下7天之前的日志-->
                <!--
                <Delete basePath="${logPath}" maxDepth="2">
                    <IfFileName glob="*/*.log" />
                    <IfLastModified age="180d" />
                </Delete>
                -->
            </DefaultRolloverStrategy>
        </RollingFile>
    </Appenders>

    <Loggers>
        <!--过滤掉springframework和hibernate的一些无用的debug信息 -->
        <!-- springframework的日志级别继承Root的日志级别，即Root为INFO，springframework即为INFO，以此类推 -->
        <AsyncLogger name="org.springframework" level="INFO"></AsyncLogger>
        <AsyncLogger name="org.springframework.boot.web.servlet" level="ERROR"></AsyncLogger>
        <AsyncLogger name="org.springframework.context.support" level="ERROR"></AsyncLogger>
        <AsyncLogger name="org.springframework.jdbc.datasource" level="INFO"></AsyncLogger>
        <AsyncLogger name="org.springframework.web.servlet" level="ERROR"></AsyncLogger>
        <AsyncLogger name="org.elasticsearch" level="INFO"></AsyncLogger>
        <AsyncLogger name="org.apache.activemq" level="INFO"></AsyncLogger>
        <AsyncLogger name="net.sf.ehcache" level="INFO"></AsyncLogger>
        <AsyncLogger name="org.hibernate" level="INFO"></AsyncLogger>

        <AsyncRoot level="INFO" includeLocation="true">
            <AppenderRef ref="Console"/>
            <AppenderRef ref="RollingFile"/>
        </AsyncRoot>
    </Loggers>

</configuration>
```

log4j2-spring-test.xml
```xml
<!--设置log4j2的自身log级别为warn -->
<configuration status="WARN" monitorInterval="300">
    <properties>
        <property name="logPath">${sys:LOG_PATH}</property>
    </properties>
    <Appenders>
        <Console name="Console" target="SYSTEM_OUT">
            <PatternLayout pattern="[%d{yyyy-MM-dd HH:mm:ss:SSS}] [%p] - %l - %m%n"/>
        </Console>

        <RollingFile name="RollingFileInfo" fileName="${logPath}/info.log"
                     filePattern="${logPath}/$${date:yyyy-MM-dd}/info-%d{yyyy-MM-dd}-%i.log">
            <Filters>
                <ThresholdFilter level="WARN" onMatch="DENY" onMismatch="NEUTRAL"/>
                <ThresholdFilter level="INFO" onMatch="ACCEPT" onMismatch="DENY"/>
            </Filters>
            <PatternLayout pattern="[%d{yyyy-MM-dd HH:mm:ss:SSS}] [%p] - %l - %m%n"/>
            <Policies>
                <TimeBasedTriggeringPolicy/>
                <SizeBasedTriggeringPolicy size="20 MB"/>
            </Policies>
            <DefaultRolloverStrategy max="1000"/>
        </RollingFile>

        <RollingFile name="RollingFileWarn" fileName="${logPath}/warn.log"
                     filePattern="${logPath}/$${date:yyyy-MM-dd}/warn-%d{yyyy-MM-dd}-%i.log">
            <Filters>
                <ThresholdFilter level="ERROR" onMatch="DENY" onMismatch="NEUTRAL"/>
                <ThresholdFilter level="WARN" onMatch="ACCEPT" onMismatch="DENY"/>
            </Filters>
            <PatternLayout pattern="[%d{yyyy-MM-dd HH:mm:ss:SSS}] [%p] - %l - %m%n"/>
            <Policies>
                <TimeBasedTriggeringPolicy/>
                <SizeBasedTriggeringPolicy size="20 MB"/>
            </Policies>
            <DefaultRolloverStrategy max="1000"/>
        </RollingFile>

        <RollingFile name="RollingFileError" fileName="${logPath}/error.log"
                     filePattern="${logPath}/$${date:yyyy-MM-dd}/error-%d{yyyy-MM-dd}-%i.log">
            <Filters>
                <ThresholdFilter level="ERROR" onMatch="ACCEPT" onMismatch="DENY"/>
            </Filters>
            <PatternLayout pattern="[%d{HH:mm:ss:SSS}] [%p] - %l - %m%n"/>
            <Policies>
                <TimeBasedTriggeringPolicy/>
                <SizeBasedTriggeringPolicy size="20 MB"/>
            </Policies>
            <DefaultRolloverStrategy max="1000"/>
        </RollingFile>
    </Appenders>

    <Loggers>
        <!--过滤掉springframework和hibernate的一些无用的debug信息 -->
        <!-- springframework的日志级别继承Root的日志级别，即Root为INFO，springframework即为INFO，以此类推  additivity="false" -->
        <AsyncLogger name="org.springframework" level="INFO"></AsyncLogger>
        <AsyncLogger name="org.springframework.boot.web.servlet" level="ERROR"></AsyncLogger>
        <AsyncLogger name="org.springframework.context.support" level="ERROR"></AsyncLogger>
        <AsyncLogger name="org.springframework.jdbc.datasource" level="INFO"></AsyncLogger>
        <AsyncLogger name="org.springframework.web.servlet" level="ERROR"></AsyncLogger>
        <AsyncLogger name="org.elasticsearch" level="INFO"></AsyncLogger>
        <AsyncLogger name="org.apache.activemq" level="INFO"></AsyncLogger>
        <AsyncLogger name="net.sf.ehcache" level="INFO"></AsyncLogger>
        <AsyncLogger name="org.hibernate" level="INFO"></AsyncLogger>

        <!-- druid配置 -->
        <AsyncLogger name="druid.sql.Statement" level="INFO" additivity="true">
            <appender-ref ref="Console"/>
        </AsyncLogger>
        <AsyncLogger name="druid.sql.ResultSet" level="INFO" additivity="true">
            <appender-ref ref="Console"/>
        </AsyncLogger>

        <AsyncRoot level="INFO" includeLocation="true">
            <AppenderRef ref="Console"/>
            <AppenderRef ref="RollingFileInfo"/>
            <AppenderRef ref="RollingFileWarn"/>
            <AppenderRef ref="RollingFileError"/>
        </AsyncRoot>
    </Loggers>

</configuration>
```

log4j2-spring-prod.xml
```xml
<!--设置log4j2的自身log级别为warn -->
<configuration status="WARN" monitorInterval="300">
    <properties>
        <property name="logPath">${sys:LOG_PATH}</property>
    </properties>
    <Appenders>
        <Console name="Console" target="SYSTEM_OUT">
            <PatternLayout pattern="[%d{yyyy-MM-dd HH:mm:ss:SSS}] [%p] - %l - %m%n"/>
        </Console>

        <RollingFile name="RollingFileInfo" fileName="${logPath}/info.log"
                     filePattern="${logPath}/$${date:yyyy-MM-dd}/info-%d{yyyy-MM-dd}-%i.log">
            <Filters>
                <ThresholdFilter level="WARN" onMatch="DENY" onMismatch="NEUTRAL"/>
                <ThresholdFilter level="INFO" onMatch="ACCEPT" onMismatch="DENY"/>
            </Filters>
            <PatternLayout pattern="[%d{yyyy-MM-dd HH:mm:ss:SSS}] [%p] - %l - %m%n"/>
            <Policies>
                <TimeBasedTriggeringPolicy/>
                <SizeBasedTriggeringPolicy size="20 MB"/>
            </Policies>
            <DefaultRolloverStrategy max="1000"/>
        </RollingFile>

        <RollingFile name="RollingFileWarn" fileName="${logPath}/warn.log"
                     filePattern="${logPath}/$${date:yyyy-MM-dd}/warn-%d{yyyy-MM-dd}-%i.log">
            <Filters>
                <ThresholdFilter level="ERROR" onMatch="DENY" onMismatch="NEUTRAL"/>
                <ThresholdFilter level="WARN" onMatch="ACCEPT" onMismatch="DENY"/>
            </Filters>
            <PatternLayout pattern="[%d{yyyy-MM-dd HH:mm:ss:SSS}] [%p] - %l - %m%n"/>
            <Policies>
                <TimeBasedTriggeringPolicy/>
                <SizeBasedTriggeringPolicy size="20 MB"/>
            </Policies>
            <DefaultRolloverStrategy max="1000"/>
        </RollingFile>

        <RollingFile name="RollingFileError" fileName="${logPath}/error.log"
                     filePattern="${logPath}/$${date:yyyy-MM-dd}/error-%d{yyyy-MM-dd}-%i.log">
            <Filters>
                <ThresholdFilter level="ERROR" onMatch="ACCEPT" onMismatch="DENY"/>
            </Filters>
            <PatternLayout pattern="[%d{HH:mm:ss:SSS}] [%p] - %l - %m%n"/>
            <Policies>
                <TimeBasedTriggeringPolicy/>
                <SizeBasedTriggeringPolicy size="20 MB"/>
            </Policies>
            <DefaultRolloverStrategy max="1000"/>
        </RollingFile>
    </Appenders>

    <Loggers>
        <!--过滤掉springframework和hibernate的一些无用的debug信息 -->
        <!-- springframework的日志级别继承Root的日志级别，即Root为INFO，springframework即为INFO，以此类推  additivity="false" -->
        <AsyncLogger name="org.springframework" level="INFO"></AsyncLogger>
        <AsyncLogger name="org.springframework.boot.web.servlet" level="ERROR"></AsyncLogger>
        <AsyncLogger name="org.springframework.context.support" level="ERROR"></AsyncLogger>
        <AsyncLogger name="org.springframework.jdbc.datasource" level="INFO"></AsyncLogger>
        <AsyncLogger name="org.springframework.web.servlet" level="ERROR"></AsyncLogger>
        <AsyncLogger name="org.elasticsearch" level="INFO"></AsyncLogger>
        <AsyncLogger name="org.apache.activemq" level="INFO"></AsyncLogger>
        <AsyncLogger name="net.sf.ehcache" level="INFO"></AsyncLogger>
        <AsyncLogger name="org.hibernate" level="INFO"></AsyncLogger>

        <!-- druid配置 -->
        <AsyncLogger name="druid.sql.Statement" level="INFO" additivity="true">
            <appender-ref ref="Console"/>
        </AsyncLogger>
        <AsyncLogger name="druid.sql.ResultSet" level="INFO" additivity="true">
            <appender-ref ref="Console"/>
        </AsyncLogger>

        <AsyncRoot level="INFO" includeLocation="true">
            <AppenderRef ref="Console"/>
            <AppenderRef ref="RollingFileInfo"/>
            <AppenderRef ref="RollingFileWarn"/>
            <AppenderRef ref="RollingFileError"/>
        </AsyncRoot>
    </Loggers>

</configuration>
```

### quartz Starter
```xml
<dependency>
    <groupId>org.xbdframework.boot</groupId>
    <artifactId>xbd-boot-starter-quartz</artifactId>
</dependency>
```

### xbd.quartz
<table>
    <tr>
        <td>配置项</td>
        <td>说明</td>
        <td>默认值</td>
    </tr>
    <tr>
        <td>dataSourceType</td>
        <td>数据源类型</td>
        <td>SYSTEM</td>
    </tr>
    <tr>
        <td>configLocation</td>
        <td>配置文件路径</td>
        <td>无</td>
    </tr>
    <tr>
        <td>schedulerName</td>
        <td>调度器名称</td>
        <td>无</td>
    </tr>
    <tr>
        <td>applicationContextSchedulerContextKey</td>
        <td>spring上下文key</td>
        <td>applicationContext</td>
    </tr>
    <tr>
        <td>useTaskExecutor</td>
        <td>是否使用异步执行器</td>
        <td>false</td>
    </tr>
    <tr>
        <td>overwriteExistingJobs</td>
        <td>是否覆盖存在的任务</td>
        <td>true</td>
    </tr>
    <tr>
        <td>autoStartup</td>
        <td>是否自动启动</td>
        <td>true</td>
    </tr>
    <tr>
        <td>startupDelay</td>
        <td>延迟启动时间</td>
        <td>5</td>
    </tr>
</table>

## xbd-boot-starter-web
web工程Starter
```xml
<dependency>
    <groupId>org.xbdframework.boot</groupId>
    <artifactId>xbd-boot-starter-web</artifactId>
</dependency>
```


